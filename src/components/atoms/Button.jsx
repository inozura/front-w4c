import React from "react";
import { Link } from "react-router-dom";

export const Button = ({ children, href }) => {
  return (
    <Link
      to={href}
      className="btn px-5 py-1 text-uppercase"
      style={{
        backgroundColor: "#17a2b8",
        color: "white",
        borderRadius: 20,
      }}
    >
      {children}
    </Link>
  );
};
