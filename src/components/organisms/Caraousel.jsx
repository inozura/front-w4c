import React from "react";
import Slider from "react-slick";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export const Caraousel = ({ data }) => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
  };
  return (
    <div
      className="d-flex py-4"
      style={{
        backgroundImage:
          "url(https://waste4change.com/official/2.8.assets/img/bg/sleek_bg_image_black.png)",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        width: "100%",
      }}
    >
      <div className="container">
        <h1 className="text-center my-5">Klien</h1>
        <Slider {...settings}>
          {data.map((item) => (
            <div className="p-4" key={item.id}>
              <div className="rounded py-3 px-3 bg-white box-sizing">
                <img
                  src={item.logo}
                  alt={item.id}
                  width="auto"
                  className="img-fluid"
                />
              </div>
              <p className="text-center">{item.name}</p>
            </div>
          ))}
        </Slider>
      </div>
    </div>
  );
};
