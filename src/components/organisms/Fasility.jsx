import React from "react";
import { Fade } from "react-reveal";

export const Fasility = ({ data }) => {
  return (
    <div className="container my-5">
      <div className="row">
        <div className="col-md-6">
          <h3>YANG ANDA DAPAT</h3>
          <p>
            Untuk jenis layanan ini, Waste4Change menyediakan fasilitas sebagai
            berikut:
          </p>
        </div>
        <div className="col-md-6">
          <div className="row">
            {data.map((item, index) => (
              <Fade top duration={900 + index * 150} key={item.id}>
                <div className="col-md-6">
                  <div className="row">
                    <div className="col-auto">
                      <img
                        src={item.image_url}
                        alt={item.id}
                        width={70}
                        height={70}
                      />
                    </div>
                    <div className="col">
                      <p>{item.name}</p>
                    </div>
                  </div>
                </div>
              </Fade>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};
