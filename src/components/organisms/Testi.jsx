import React from "react";
import ReactPlayer from "react-player";

export const Testi = ({ data }) => {
  console.log(data);
  return (
    <div className="container">
      <h1 className="text-center my-5">TESTIMONI</h1>
      <div
        style={{
          position: "relative",
          paddingTop: "56.25%",
        }}
      >
        <ReactPlayer
          style={{
            position: "absolute",
            top: 0,
            left: 0,
          }}
          url={data[0] && data[0].url_video}
          width="100%"
          height="100%"
        />
      </div>
    </div>
  );
};
