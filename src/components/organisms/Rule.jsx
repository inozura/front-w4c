import React from "react";
import { Fade } from "react-reveal";

export const Rule = ({ data }) => {
  return (
    <div className="container my-5">
      <h1 className="text-center">ALUR SAMPAH</h1>
      <p className="text-center mb-3">Pengelolaan Sampah Residu di TPA</p>
      <div className="row justify-content-center">
        {data.map((item, index) => (
          <Fade bottom duration={900 + index * 200} key={item.id}>
            <div className="col-md-4 d-flex flex-column justify-content-center align-items-center mb-4">
              <img src={item.image_url} alt={item.id} width="40%" />
              <p className="mt-4 text-center">{item.name}</p>
            </div>
          </Fade>
        ))}
      </div>
    </div>
  );
};
