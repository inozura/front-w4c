import React from "react";
import styled from "styled-components";
import { Container, Row, Col } from "grids-styled-components";
import { Button } from "../atoms/Button";

const MainContent = styled(Container)`
  background-image: url("https://waste4change.com/official/2.8.assets/img/service/responsible-waste-management/bg/header.jpg");
  height: 100vh;
  background-repeat: no-repeat;
  background-size: cover;
`;

const MyRow = styled(Row)`
  height: 100%;
`;

const MyCol = styled(Col)`
  justify-content: center;
  z-index: 999;
  flex-direction: column;
`;

const GradientBackground = styled.div`
  position: absolute;
  background-image: linear-gradient(
    100deg,
    rgba(255, 255, 255, 0.9) 0%,
    rgba(255, 255, 255, 0.6) 55%,
    rgba(255, 255, 255, 0) 75%
  );
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

const MyH1 = styled.h1`
  color: black;
`;

export const Hero = () => {
  return (
    <MainContent>
      <MyRow>
        <GradientBackground />
        <MyCol type="col-md-6">
          <Container>
            <MyH1>Responsible Waste Management</MyH1>
            <p>
              Sistem manajemen sampah yang 100% menyeluruh untuk perusahaan,
              gedung, dan pelaku bisnis dalam rangka mengurangi jumlah timbunan
              sampah yang berakhir di TPA.
            </p>
            <Button href="/">dapatkan proposal</Button>
          </Container>
        </MyCol>
      </MyRow>
    </MainContent>
  );
};
