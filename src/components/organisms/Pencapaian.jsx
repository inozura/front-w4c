import React from "react";
import CountUp from "react-countup";

export const Pencapaian = ({ data, className, style }) => {
  console.log(data);
  return (
    <div
      className={`container shadow p-3 mb-5 bg-body rounded position-relative ${className}`}
      style={style}
    >
      <div className="row">
        <div className="col-md-4">
          <h1 className="text-center">PENCAPAIAN KAMI</h1>
        </div>
        <div className="col-md-8">
          <div className="row justify-content-center text-center">
            <div className="col-md-3 flex-column justify-content-center align-items-center">
              <CountUp
                className="fs-1"
                end={data[0] && data[0].clients}
                duration={1}
              />
              <p>Klien</p>
            </div>
            <div className="col-md-3 flex-column justify-content-center align-items-center">
              <CountUp
                className="fs-1"
                end={data[0] && data[0].trash_weight}
                duration={1}
              />
              <p>Sampah</p>
            </div>
            <div className="col-md-3 flex-column justify-content-center align-items-center">
              <CountUp
                className="fs-1"
                end={data[0] && data[0].cities}
                duration={1}
              />
              <p>Kota</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
