import React from "react";
import { Fade } from "react-reveal";

export const Advantage = ({ data }) => {
  return (
    <div className="container my-5">
      <h1 className="text-center mb-5">Keuntungan</h1>
      <div className="row">
        {data.map((item, index) => (
          <Fade left duration={900 + index * 200} key={item.id}>
            <div className="col-md-6 my-3">
              <div className="row align-items-center">
                <div className="col-auto justify-content-center">
                  <img
                    src="https://waste4change.com/official/2.8.assets/img/icons/check-circle.png"
                    alt={`img-${index}`}
                  />
                </div>
                <div className="col">{item.desc}</div>
              </div>
            </div>
          </Fade>
        ))}
      </div>
    </div>
  );
};
