import styled from "@emotion/styled";
import React from "react";
import { Fade } from "react-reveal";

const MyDiv = styled.div`
  padding: 20px !important;
  margin-left: -100px;
  background-image: linear-gradient(
    90deg,
    rgba(11, 144, 185, 0.9),
    rgba(19, 182, 127, 0.9)
  );

  @media (max-width: 768px) {
    margin: 0px;
  }
`;

export const About = () => {
  return (
    <div className="container my-4">
      <div className="row">
        <div className="col-md-6">
          <Fade left duration={900}>
            <img
              className="img-fluid"
              style={{ objectFit: "cover" }}
              src="https://waste4change.com/official/2.8.assets/img/service/responsible-waste-management/bg/about.jpg"
              alt="img"
            />
          </Fade>
        </div>
        <div className="col-md-6">
          <Fade top duration={1100}>
            <h3>TENTANG</h3>
            <h2>Tidak Ada Lagi Sampah Yang Tercampur</h2>
          </Fade>
          <Fade bottom duration={1300}>
            <MyDiv>
              <p className="text-white">
                Produksi sampah merupakan sesuatu yang tidak bisa dihindari.
                Mengurangi sampah sendiri bukanlah perkara yang mudah. Hal yang
                paling sederhana, mudah, dan penting yang dapat kita lakukan
                setelah menghasilkan sampah adalah memisahkan sampah organik dan
                sampah anorganik.
              </p>
              <p className="text-white">
                Dengan layanan kami yang bernama Responsible Waste Management
                (RWM), Waste4Change menyediakan manajemen sampah untuk
                mengurangi jumlah timbulan sampah yang menumpuk di TPA.
              </p>
            </MyDiv>
          </Fade>
        </div>
      </div>
    </div>
  );
};
