import React from "react";
import Slider from "react-slick";

export const Armada = ({ data }) => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    arrows: true,
  };
  return (
    <div className="container py-5">
      <h1 className="text-center my-5">ARMADA</h1>
      <Slider {...settings}>
        {data.map((item) => (
          <div className="p-4" key={item.id}>
            <div className="bg-white rounded shadow p-3">
              <div className="py-3 px-3">
                <img
                  src={item.image_url}
                  alt={item.id}
                  width="auto"
                  className="img-fluid"
                />
              </div>
              <p className="text-center">{item.name}</p>
              <p className="text-center">Kapasitas {item.capacity}</p>
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
};
