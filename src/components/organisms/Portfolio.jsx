import React from "react";
import { Fade } from "react-reveal";

export const Portfolio = ({ data }) => {
  return (
    <div className="container my-5">
      <h1 className="text-center mb-5">SOROTAN PORTOFOLIO</h1>
      <div className="row">
        {data.map((item, index) => (
          <Fade bottom duration={1000 + index * 150} key={item.id}>
            <div className="col-md-6 px-3">
              <div className="shadow rounded bg-white">
                <div className="row">
                  <div className="col-md-6 px-5 py-5">
                    <img
                      src={item.logo}
                      alt={item.id}
                      max-height={80}
                      max-width={100}
                    />
                    <h3>{item.name}</h3>
                    <p>{item.address}</p>
                    <div className="row">
                      <div className="col-auto">
                        <img
                          src="https://waste4change.com/official/2.8.assets/img/icons/check-circle-o.png"
                          alt=""
                          style={{ maxWidth: 20 }}
                        />
                      </div>
                      <div className="col">
                        <span className="d-block">Durasi Layanan</span>
                        <strong>{item.duration ? item.duration : "-"}</strong>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-auto">
                        <img
                          src="https://waste4change.com/official/2.8.assets/img/icons/check-circle-o.png"
                          alt=""
                          style={{ maxWidth: 20 }}
                        />
                      </div>
                      <div className="col">
                        <span className="d-block">Penandatanganan MoU</span>
                        <strong>{item.signature ? item.signature : "-"}</strong>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-auto">
                        <img
                          src="https://waste4change.com/official/2.8.assets/img/icons/refresh-clock.png"
                          alt=""
                          style={{ maxWidth: 20 }}
                        />
                      </div>
                      <div className="col">
                        <span className="d-block">Jadwal Pengangkutan</span>
                        <strong>{item.schedule ? item.schedule : "-"}</strong>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-auto">
                        <img
                          src="https://waste4change.com/official/2.8.assets/img/icons/refresh.png"
                          alt=""
                          style={{ maxWidth: 20 }}
                        />
                      </div>
                      <div className="col">
                        <span className="d-block">Penandatanganan MoU</span>
                        <strong>{item.recycled ? item.recycled : "-"}</strong>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <img
                      src="https://waste4change.com/official/2.8.assets/img/service/responsible-waste-management/portfolio/l-vida.jpg"
                      alt="img"
                      width="100%"
                      height="100%"
                      style={{ objectFit: "cover" }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </Fade>
        ))}
      </div>
    </div>
  );
};
