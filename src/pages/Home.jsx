import React, { useEffect, useState } from "react";
import { About } from "../components/organisms/About";
import { Advantage } from "../components/organisms/Advantage";
import { Armada } from "../components/organisms/Armada";
import { Caraousel } from "../components/organisms/Caraousel";
import { Fasility } from "../components/organisms/Fasility";
import { Hero } from "../components/organisms/Hero";
import { Pencapaian } from "../components/organisms/Pencapaian";
import { Portfolio } from "../components/organisms/Portfolio";
import { Rule } from "../components/organisms/Rule";
import { Testi } from "../components/organisms/Testi";

import { Fade } from "react-reveal";

export const Home = () => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const data = await fetch(
          "https://back-w4c.000webhostapp.com/api/responsible"
        ).then((res) => res.json());
        setData(data);
      } catch (err) {
        console.log(err);
      } finally {
        setIsLoading(false);
      }
    };
    fetchData();
    return () => {
      setData([]);
    };
  }, []);

  return isLoading ? (
    <div
      style={{
        display: "flex",
        width: "100%",
        height: "100vh",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <p>Loading</p>
    </div>
  ) : (
    <div style={{ overflow: "hidden" }}>
      <Hero />
      <Fade bottom duration={900}>
        <Pencapaian data={data.achievement} style={{ marginTop: -80 }} />
      </Fade>
      <About />
      <Advantage data={data.advantage} />
      <Fasility data={data.fasility} />
      <Rule data={data.rule} />
      <Portfolio data={data.portfolio} />
      <Caraousel data={data.clients} />
      <Testi data={data.testi} />
      <Armada data={data.fleet} />
    </div>
  );
};
