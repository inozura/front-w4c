import React from "react";
import { Switch, Route } from "react-router-dom";
import { Home, Responsible } from "./pages";

export const routes = ({ children }) => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/responsible" component={Responsible} />
    </Switch>
  );
};
