import { BrowserRouter as Router } from "react-router-dom";
import { Navbar } from "./components/organisms/Navbar";
import { routes as Routes } from "./routes";

function App() {
  return (
    <Router>
      <Navbar />
      <Routes />
    </Router>
  );
}

export default App;
